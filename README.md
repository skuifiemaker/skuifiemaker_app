# Skuifiemaker

## Installing

Navigate into the repo and do: `pip install -e .`

## Dependencies:

- lualatex
- python3.6.1 or later

### Python modules:

- click
- wxPython

(Probably others, too, but that's all I had to install on top of Miniconda3 to get it to work.)

### Tex/Latex packages:

TODO update this

On a texlive installation with at least the "basic" scheme:
```
tlmgr install elpres xcolor pagecolor microtype nopageno luaotfload lm
```

## Building an exe:

You need the above requirements, plus:

- pyinstaller: pip install pyinstaller
- Microsoft Visual Studio Build Tools

Then run this:

```
pyinstaller make_slides_wx.py --add-data="songs.sqlite;."
```

The `dist` folder now contains all you need to run it on a Windows PC.

## PPT conversion:

- https://github.com/ashafaei/pdf2pptx/blob/master/pdf2pptx.sh
- http://forsyte.at/people/konnov/beamer-to-powerpoint/

## Leesstof:

- http://www.enigstetroos.org/psalmberymingscent.htm
- https://proregno.files.wordpress.com/2015/03/totius-oor-die-psalm-melodiee.pdf
- http://williejonker.co.za/19870328-2/
- https://verbumetecclesia.org.za/index.php/ve/article/download/1190/1631

