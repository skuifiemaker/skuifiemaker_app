"""
Filesystem utilities
"""

import os
import errno


def mkdir(path):
    """
    Copied from my M project (Dolf)

    Create a directory (including parents) if it doesn't exist.

    :param path: The path of the directory to create.

    """
    try:
        os.makedirs(path)
    except OSError as e:
        # Pass if the directory exists. Raise on all other errors.
        if e.errno != errno.EEXIST:
            raise


def split_path(path, max_ext=None):
    """
    Copied from my M project (Dolf)

    No trailing slashes on returned directory

    :param str path: Any path to a file or folder

    :param int max_ext: The maximum number of extensions (parts of name separated by period).
        Any more than this will be considered as part of the name. Default: None, meaning no limit.

    :return: tuple (directory, name, extensions)

    >>> split_path('/home/dolf/asdf.csv')
    ('/home/dolf', 'asdf', '.csv')

    >>> split_path('/home/dolf/asdf.csv.gz')
    ('/home/dolf', 'asdf', '.csv.gz')

    >>> split_path('/home/dolf/asdf.csv.gz.bak')
    ('/home/dolf', 'asdf', '.csv.gz.bak')

    >>> split_path('/home/dolf/.bashrc')
    ('/home/dolf', '.bashrc', '')

    >>> split_path('/home/dolf/.bashrc.bak')
    ('/home/dolf', '.bashrc', '.bak')

    >>> split_path('asdf.csv')
    ('', 'asdf', '.csv')

    >>> split_path('asdf.csv.gz')
    ('', 'asdf', '.csv.gz')

    >>> split_path('asdf.csv.gz.bak')
    ('', 'asdf', '.csv.gz.bak')

    >>> split_path('.bashrc')
    ('', '.bashrc', '')

    >>> split_path('.bashrc.bak')
    ('', '.bashrc', '.bak')

    >>> split_path('/home/dolf/')
    ('/home/dolf', '', '')

    >>> split_path('/home/dolf')
    ('/home/dolf', '', '')

    >>> split_path('.')
    ('.', '', '')

    >>> split_path('./')
    ('.', '', '')

    >>> split_path('20180119_1813_coetzenburg_datum_converted.gpx')
    ('', '20180119_1813_coetzenburg_datum_converted', '.gpx')

    >>> split_path('/home/dolf/m/toetse/20171222/jan.local.20171222142044.bin', max_ext=1)
    ('/home/dolf/m/toetse/20171222', 'jan.local.20171222142044', '.bin')

    """

    if type(path) is not str:
        raise TypeError("Path must be a string.")

    # If it ends on a separator, treat it as a directory.
    if path.endswith(os.sep):
        # Remove trailing separator
        return path.rstrip(os.sep), "", ""

    # If it is an existing directory, treat it as such.
    if os.path.isdir(path):
        return path, "", ""

    directory, name = os.path.split(path)
    extensions = []
    while max_ext is None or len(extensions) < max_ext:
        name, ext = os.path.splitext(name)
        if not ext:
            break
        extensions.append(ext)

    return directory, name, "".join(reversed(extensions))
