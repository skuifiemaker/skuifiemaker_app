#!/usr/bin/env python3
"""
Make slides
"""

import click
from skuifiemaker_common.lyrics_db import LyricsDB
from ui import parse_song_reference
from tex import lyrics_tex_document, make_pdf


@click.command()
@click.argument('reference')
@click.argument('output-filename')
@click.option('--tmp-dir', required=False, default='./tmp')
@click.option('--author', required=False, default=None)
@click.option('--year', required=False, default=None)
def main(reference, tmp_dir, author, year, output_filename):
    songbook_name, song_number, stanza_numbers = parse_song_reference(reference)
    with LyricsDB('songs.sqlite') as db:

        # First determine the versification.
        if author and year:
            # There will only be one.
            v = db.get_versification(author, year)
        elif author and not year:
            # Use the first one by that author.
            v = db.get_versifications_by_author(author)[0]
        elif year and not author:
            # Use the first one from that year.
            v = db.get_versifications_by_year(year)[0]
        else:
            # Use the first one from that song.
            v = db.get_versifications_of_song(
                song_book=songbook_name,
                song_number=song_number,
            )[0]

    make_pdf(
        tex_source=lyrics_tex_document(
            song_book=songbook_name,
            song_number=song_number,
            stanza_numbers=stanza_numbers,
            versification=v,
        ),
        output_filename=output_filename,
        tmp_directory=tmp_dir,
    )


if __name__ == '__main__':
    main()
