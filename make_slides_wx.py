import configparser
import tempfile
from multiprocessing.pool import Pool, AsyncResult
from multiprocessing import freeze_support
from pathlib import Path
from typing import List
import click
import os
import time
import wx
import wx.grid
import tex
from skuifiemaker_common.lyrics_db import LyricsDB, NotFoundError
from ui import parse_song_reference
from skuifiemaker_common.versifications import Versification
from wxutil import titled, stacked


def make_pdf(parsed: List, versifications: List, stanzas: List, output_filename: str, tmp_directory: str):
    slides = []
    p: tuple
    v: Versification
    s: dict
    for p, v, s in zip(parsed, versifications, stanzas):
        # Skip invalid entries
        if s is None or v is None or p is None:
            continue

        song_book, song_number, requested_stanzas, available_versifications = p
        slides.extend(tex.lyrics_tex_slides(
            song_book=song_book,
            song_number=song_number,
            stanza_numbers=s['valid'],
            versification=v,
        ))

    return tex.make_pdf(
        tex_source=tex.document(slides),
        output_filename=output_filename,
        tmp_directory=tmp_directory,
    )


def format_stanza_result_line(stanzas: dict) -> str:
    if stanzas is not None:
        if len(stanzas['invalid']):
            return "Invalid: {}".format(", ".join(str(s) for s in stanzas['invalid']))
        if len(stanzas['valid']):
            return ", ".join(str(s) for s in stanzas['valid'])

    return "No stanzas"


class TopFrame(wx.Frame):
    """
    The main frame for the GUI
    """

    @property
    def target_file(self):
        return self._target_file.format(
            timestamp=time.strftime("%Y%m%d_%H%M%S", time.localtime()),
        )

    @target_file.setter
    def target_file(self, value):
        self._target_file = value

    @target_file.deleter
    def target_file(self):
        del self._target_file

    def __init__(self, *args, pool: Pool, **kw):
        # ensure the parent's __init__ is called
        super(TopFrame, self).__init__(*args, **kw)

        self.pool = pool

        self.CreateStatusBar()
        self.SetStatusText("Ready.")

        self.text_input = wx.TextCtrl(self, style=wx.TE_MULTILINE, size=wx.Size(200, 200))
        self.versification_pickers_sizer = stacked(
            widgets=[],
            orient=wx.VERTICAL
        )
        self.stanzas_text = wx.StaticText(self, style=wx.TE_MULTILINE, size=wx.Size(200, 200))

        input_sizer = stacked(widgets=[
            titled(parent=self, widget=self.text_input, title="Input"),
            titled(parent=self, widget=self.versification_pickers_sizer, title="Versification"),
            titled(parent=self, widget=self.stanzas_text, title="Stanzas"),
        ], orient=wx.HORIZONTAL, proportion=1)

        self.out_dir_picker = wx.DirPickerCtrl(self, message="Select the directory in which to place the output file")
        # TODO: save the last used path from when the program was last used.

        self.tmp_dir_picker = wx.DirPickerCtrl(self, message="Select the directory to use as temporary LaTeX workspace")
        # TODO: save the last used path from when the program was last used.

        self.filename_text = wx.TextCtrl(self)
        self.filename_text.SetToolTip(
            """The name of the output file.

If this does not end in ".pdf", the extension will be added.
If this is empty, the name will be a timestamp like "%Y%m%d_%H%M%S.pdf"."""
        )

        self._target_file = None
        self.calculated_target_file_text = wx.StaticText(self)

        options_sizer = stacked(widgets=[
            titled(
                parent=self,
                widget=self.out_dir_picker,
                title="Output directory:",
                orient=wx.HORIZONTAL,
            ),
            titled(
                parent=self,
                widget=self.filename_text,
                title="Output filename:",
                orient=wx.HORIZONTAL,
            ),
            self.calculated_target_file_text,
            titled(
                parent=self,
                widget=self.tmp_dir_picker,
                title="Temporary directory:",
                orient=wx.HORIZONTAL,
            ),
        ], flag=wx.EXPAND)

        self.make_pdf_button = wx.Button(self, label='Make PDF')

        buttons_sizer = wx.BoxSizer(wx.HORIZONTAL)
        buttons_sizer.Add(self.make_pdf_button)

        top_sizer = wx.BoxSizer(wx.VERTICAL)
        top_sizer.Add(input_sizer, flag=wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND)
        top_sizer.Add(options_sizer, flag=wx.EXPAND)
        top_sizer.Add(buttons_sizer, flag=wx.ALIGN_CENTER_HORIZONTAL)
        top_sizer.SetSizeHints(self)
        self.SetSizer(top_sizer)

        self.load_state()
        self.calculate_target_file()

        self.input_modified_timer = wx.Timer(self)
        self.Bind(
            event=wx.EVT_TIMER,
            handler=self.parse_input,
            source=self.input_modified_timer,
        )
        self.Bind(
            event=wx.EVT_TEXT,
            handler=self.input_has_changed,
            source=self.text_input,
        )

        self.versification_picked_timer = wx.Timer(self)
        self.Bind(
            event=wx.EVT_TIMER,
            handler=self.update_stanzas,
            source=self.versification_picked_timer,
        )

        self.pdf_poll_timer = wx.Timer(self)
        self.Bind(
            event=wx.EVT_TIMER,
            handler=self.make_pdf_poll,
            source=self.pdf_poll_timer,
        )
        self.Bind(
            event=wx.EVT_BUTTON,
            handler=self.make_pdf_button_clicked,
            source=self.make_pdf_button,
        )

        self.filename_timer = wx.Timer(self)
        self.Bind(
            event=wx.EVT_TIMER,
            handler=self.calculate_target_file,
            source=self.filename_timer,
        )
        self.Bind(
            event=wx.EVT_TEXT,
            handler=self.filename_has_changed,
            source=self.filename_text,
        )
        self.Bind(
            event=wx.EVT_DIRPICKER_CHANGED,
            handler=self.filename_has_changed,
            source=self.out_dir_picker,
        )

        self.Bind(
            event=wx.EVT_CLOSE,
            handler=self.on_close_frame,
        )

        # TODO: use "Parsed" objects, maybe namedtuples?
        self.parsed = []
        self.versification_pickers = []
        self.stanzas = []
        self.make_pdf_async_result = None  # type: AsyncResult or None

    def input_has_changed(self, event: wx.CommandEvent) -> None:
        # (re)start the timer every time the text changes.
        # When the user stops typing, the timer runs out, and the input is parsed.
        self.input_modified_timer.StartOnce(500)

    def filename_has_changed(self, event: wx.CommandEvent) -> None:
        # (re)start the timer every time the filename changes.
        # When the user stops typing, the timer runs out, and the new output filename is calculated.
        self.filename_timer.StartOnce(500)

    def parse_input(self, event):
        text = self.text_input.GetValue()
        input_refs = (ref for ref in (line.strip() for line in text.split('\n')) if len(ref))

        parsed = []
        for input_ref in input_refs:
            try:
                songbook_name, song_number, stanza_numbers = parse_song_reference(input_ref)
                try:
                    with LyricsDB('songs.sqlite') as db:
                        versifications = db.get_versifications_of_song(
                            song_book=songbook_name,
                            song_number=song_number
                        )
                    parsed.append((
                        songbook_name,
                        song_number,
                        stanza_numbers,
                        versifications,
                    ))
                except NotFoundError:
                    parsed.append("Cannot find {} {}".format(songbook_name, song_number))
            except ValueError as e:
                parsed.append(str(e))

        self.parsed = parsed
        self.update_versification_pickers()

    def make_pdf_button_clicked(self, event):
        if len(self.parsed):
            self.make_pdf_button.Disable()
            self.SetStatusText("Making PDF...")

            # Get the chosen versifications.
            versifications = []
            for p in self.versification_pickers:
                if isinstance(p, wx.Choice):
                    # TODO: subclass wx.Choice and add a "getSelectedVersification" method.
                    versifications.append(p.versifications[p.CurrentSelection])
                else:
                    versifications.append(None)

            # Start the job to make the PDF.
            self.make_pdf_async_result = self.pool.apply_async(
                func=make_pdf,
                kwds={
                    'parsed': self.parsed,
                    'versifications': versifications,
                    'stanzas': self.stanzas,
                    'output_filename': self.target_file,
                    'tmp_directory': self.tmp_dir_picker.GetPath(),
                },
            )
            self.pdf_poll_timer.StartOnce(200)
        else:
            self.SetStatusText("Nothing to do.")

    def make_pdf_poll(self, event):
        if self.make_pdf_async_result.ready():
            output_path = self.make_pdf_async_result.get()
            self.SetStatusText("PDF written to: {}".format(output_path))
            self.make_pdf_button.Enable()
        else:
            self.pdf_poll_timer.StartOnce(200)

    def calculate_target_file(self, event=None):
        filename = self.filename_text.GetValue()

        if not len(filename):
            filename = '{timestamp}'

        if filename[:-4].lower() != '.pdf':
            filename += '.pdf'

        self.target_file = os.path.realpath(os.path.join(self.out_dir_picker.GetPath(), filename))
        f = None
        try:
            f = self.target_file
        except:
            self.filename_text.SetBackgroundColour((255, 0, 0))
            self.make_pdf_button.Disable()

        if f:
            self.filename_text.SetBackgroundColour(None)
            self.calculated_target_file_text.SetLabel("PDF will be written to: {}".format(f))
            self.make_pdf_button.Enable()  # FIXME: this might cause the button to be enabled while the PDF is still being written. TODO: centralise the button state logic somewhere.

    def on_close_frame(self, event):
        self.save_state()
        self.Destroy()

    def save_state(self):
        state = configparser.ConfigParser()
        state['LAST'] = {
            'out_dir': self.out_dir_picker.GetPath(),
            'tmp_dir': self.tmp_dir_picker.GetPath(),
            'filename': self.filename_text.GetValue(),
        }
        with open('state.ini', 'w') as configfile:
            state.write(configfile)

    def load_state(self):
        state = configparser.ConfigParser()
        state.read('state.ini')

        last = {
            'out_dir': str(Path.home()),
            'tmp_dir': tempfile.gettempdir(),
            'filename': '',
        }
        try:
            last.update(state['LAST'])
        except KeyError:
            pass

        self.out_dir_picker.SetPath(last['out_dir'])
        self.tmp_dir_picker.SetPath(last['tmp_dir'])
        self.filename_text.SetValue(last['filename'])

    def update_versification_pickers(self):
        while len(self.versification_pickers_sizer.GetChildren()):
            self.versification_pickers_sizer.Hide(0)
            self.versification_pickers_sizer.Remove(0)

        versification_picker: wx.Choice or None
        for versification_picker in self.versification_pickers:
            if versification_picker is not None:
                self.Unbind(
                    event=wx.EVT_CHOICE,
                    source=versification_picker,
                )
        self.versification_pickers = []

        for i, p in enumerate(self.parsed):
            if isinstance(p, tuple):
                # This is the parsed reference.
                songbook_name, song_number, stanza_numbers, versifications = p
                versification_picker = wx.Choice(parent=self, choices=["{}, {}".format(v.author, v.year) for v in versifications])
                versification_picker.versifications = versifications
                versification_picker.SetSelection(0)
                self.Bind(
                    event=wx.EVT_CHOICE,
                    handler=self.versification_has_changed,
                    source=versification_picker,
                )
                self.versification_pickers_sizer.Add(titled(
                    parent=self,
                    widget=versification_picker,
                    title="{} {}: ".format(songbook_name, song_number),
                    orient=wx.HORIZONTAL,
                ))
            else:
                # This is a message for the user.
                self.versification_pickers_sizer.Add(wx.StaticText(self, label=p))
                versification_picker = None

            self.versification_pickers.append(versification_picker)
            self.Layout()

        # Trigger an update of the results
        self.versification_has_changed()

    def versification_has_changed(self, event: wx.CommandEvent = None) -> None:
        # (re)start the timer every time a new versification is selected.
        self.versification_picked_timer.StartOnce(500)

    def update_stanzas(self, event: wx.CommandEvent) -> None:
        self.stanzas = []

        versification_picker: wx.Choice
        for i, versification_picker in enumerate(self.versification_pickers):
            if isinstance(versification_picker, wx.Choice):
                selected_index = versification_picker.CurrentSelection
                chosen_versification = versification_picker.versifications[selected_index]
                song_book, song_number, requested_stanzas, available_versifications = self.parsed[i]

                # TODO: are we allowed to query the DB in this thread?
                with LyricsDB('songs.sqlite') as db:
                    available_stanzas = db.get_all_stanza_numbers(
                        song_book=song_book,
                        song_number=song_number,
                        versification=chosen_versification,
                    )
                if requested_stanzas is None:
                    # This means get ALL stanzas
                    valid_stanzas = available_stanzas
                    invalid_stanzas = []
                else:
                    valid_stanzas = [s for s in requested_stanzas if s in available_stanzas]
                    invalid_stanzas = [s for s in requested_stanzas if s not in available_stanzas]

                stanzas = {
                    'valid': valid_stanzas,
                    'invalid': invalid_stanzas,
                }
            else:
                stanzas = None

            self.stanzas.append(stanzas)

        # Update the display.
        self.stanzas_text.SetLabel("\n".join([format_stanza_result_line(s) for s in self.stanzas]))


@click.command()
def main():
    app = wx.App()
    with Pool() as p:
        frm = TopFrame(None, title='Skuifiemaker', pool=p)
        frm.Show()
        app.MainLoop()


if __name__ == '__main__':
    freeze_support()
    main()

