from setuptools import setup

setup(
    name='skuifiemaker',
    version='0.1',
    py_modules=[
        'make_slides',
        'make_slides_wx',
    ],
    install_requires=[
        'Click', 'wxPython',
    ],
    entry_points='''
        [console_scripts]
        make_slides=make_slides:main
        make_slides_wx=make_slides_wx:main
    ''',
)
