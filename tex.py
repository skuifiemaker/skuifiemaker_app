"""
TeX-related functions.
"""
from typing import Sequence, Generator
import numpy as np
from skuifiemaker_common.lyrics_db import LyricsDB, NotFoundError
from skuifiemaker_common.versifications import Versification
from warnings import warn
from fs import mkdir, split_path
import os
from subprocess import CalledProcessError, check_call
from shutil import copyfile
from shutil import rmtree

pre = r"""\documentclass[12pt,sansfont,16x9]{elpres}
\usepackage{xcolor}
\usepackage{pagecolor}
\usepackage{microtype}
\pagecolor{black}
\color{white}
\usepackage{nopageno}
\begin{document}
\centering"""

post = r"\end{document}"


def make_pdf(tex_source: str, output_filename: str, tmp_directory: str, program: str = 'lualatex') -> str:
    """

    :param tex_source:

    :param output_filename:

    :param tmp_directory:

    :param program:

    :return string: Full path to the created file.

    """
    output_filename = os.path.realpath(output_filename)

    # Automatically add the .pdf extension if it is missing
    if output_filename[-4:].lower() != '.pdf':
        output_filename += ".pdf"

    tmp_directory = os.path.realpath(tmp_directory)
    o_dir, o_name, o_ext = split_path(output_filename, max_ext=1)

    # Make an empty working directory under tmp
    working_dir = os.path.join(tmp_directory, o_name)
    try:
        rmtree(working_dir)
    except FileNotFoundError:
        pass  # It's not a problem if the working dir doesn't exist yet.
    mkdir(working_dir)

    # Dump the tex source to the working directory
    tex_source_filename = os.path.join(working_dir, "{}.tex".format(o_name))
    with open(file=tex_source_filename, mode='w', encoding='utf-8') as f:
        f.write(tex_source)

    # Check for characters in the filename that might upset Windows
    if " " in tex_source_filename:
        warn("The path of the TeX source file contains a space, "
             "which might prevent TeX from working properly on Windows.")

    # Compile the source
    stdout_filename = os.path.join(working_dir, 'latex.stdout')
    stderr_filename = os.path.join(working_dir, 'latex.stderr')
    try:
        with open(stdout_filename, 'w') as stdout, open(stderr_filename, 'w') as stderr:
            check_call(
                args=[program, '-interaction=nonstopmode', tex_source_filename],
                cwd=working_dir,
                stderr=stderr,
                stdout=stdout,
            )
    except CalledProcessError:
        raise RuntimeError(
            "Failed to create PDF document. The output of {} was saved to:\n"
            "- STDOUT: {}\n"
            "- STDERR: {}".format(
                program,
                stdout_filename,
                stderr_filename,
            )
        )
    except FileNotFoundError:
        raise FileNotFoundError("Failed to create PDF document. The program {} was not found.".format(program))

    # Copy the result to the output directory
    mkdir(o_dir)
    try:
        copyfile(src=os.path.join(working_dir, "{}.pdf".format(o_name)), dst=output_filename)
    except FileNotFoundError:
        raise FileNotFoundError(
            "{} was executed, but no PDF was created. "
            "Check the output files for errors:\n"
            "- STDOUT: {}\n"
            "- STDERR: {}".format(
                program,
                stdout_filename,
                stderr_filename,
            )
        )

    return output_filename


def lyrics_tex_document(
        song_book: str,
        song_number: int,
        stanza_numbers: Sequence[int] or None,
        versification: Versification
) -> str:
    return document(slides=lyrics_tex_slides(
        song_book=song_book,
        song_number=song_number,
        stanza_numbers=stanza_numbers,
        versification=versification,
    ))


def lyrics_tex_slides(
        song_book: str,
        song_number: int,
        stanza_numbers: Sequence[int] or None,
        versification: Versification
) -> Generator[str, None, None]:
    with LyricsDB('songs.sqlite') as p:
        try:
            title = p.get_title(
                song_book=song_book,
                song_number=song_number,
            )
        except NotFoundError:
            title = "{} {}".format(song_book, song_number)

        if stanza_numbers is None:
            stanza_numbers = p.get_all_stanza_numbers(
                song_book=song_book,
                song_number=song_number,
                versification=versification,
            )
        for stanza_number in stanza_numbers:
            yield lyrics_slide(
                top_left="{}: {}".format(title, stanza_number),
                top_right="{}, {}".format(
                    versification.author,
                    versification.year,
                ),
                lines=p.get_stanza(
                    song_book=song_book,
                    song_number=song_number,
                    versification=versification,
                    stanza_number=stanza_number
                ).lyrics.split("\n")
            )


def lyrics_slide(top_left: str, top_right: str, lines: Sequence[str]) -> str:
    # Split lines with slashes
    # TODO: Split these when importing to the DB instead of here?
    new_lines = []
    for line in lines:
        if '/' in line:
            new_lines.extend(line.split('/'))
        else:
            new_lines.append(line)
    lines = new_lines

    # Split long lines. The maximum number of chars per line depends on the font size,
    # which depends on the number of lines.
    max_chars_per_line = 45
    lines = split_long_lines(lines, max_chars_per_line)

    if len(lines) <= 4:
        size = r'\Huge'
    elif len(lines) <= 6:
        size = r'\huge'
    elif len(lines) <= 8:
        size = r'\LARGE'
    else:
        # Split the slide
        max_lines_per_slide = 6
        n_slides = int(np.ceil(len(lines) / float(max_lines_per_slide)))
        n_lines_per_slide = len(lines) / float(n_slides)
        breakpoints = n_lines_per_slide * np.arange(n_slides + 1)

        return "\n".join((lyrics_slide(
            top_left="{} ({}/{})".format(top_left, i + 1, n_slides),
            top_right=top_right,
            lines=[l for j, l in enumerate(lines) if breakpoints[i] <= j < breakpoints[i+1]]
        ) for i in range(n_slides)))

    if len(top_right) and len(top_left):
        psli_title = "{} \\hfill {}".format(
            top_left,
            top_right,
        )
    elif len(top_right):
        psli_title = "\\hfill {}".format(
            top_right,
        )
    else:
        psli_title = "{}".format(
            top_left,
        )

    return r"""\begin{{psli}}[{title}]
{size}
{lines}
\end{{psli}}""".format(
        size=size,
        title=psli_title,
        lines=r" \\ ".join(lines)
    )


def split_long_lines(lines: Sequence[str], max_chars_per_line: int) -> Sequence[str]:
    new_lines = []
    for line in lines:
        if len(line) > max_chars_per_line:
            # Split line approximately in the middle.
            words = line.split()
            new_lines.append(' '.join(words[:len(words) // 2]))
            new_lines.append(' '.join(words[len(words) // 2:]))
        else:
            new_lines.append(line)
    return new_lines


def document(slides):
    return "{}\n{}\n{}".format(
        pre,
        "\n".join(slides),
        post,
    )
