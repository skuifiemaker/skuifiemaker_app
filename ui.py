"""
User interactions
"""

from typing import Tuple, List
import re


re_songbook_names = {
    'ps': r'ps(alm(s)?)?',
    'sb': r'((sb)|(skrifb(er(ym(ing(s)?)?)?)?))',
    'ges': r'ges(ang(e)?)?',
    'lied': r'l(ied(ere|boek)?)?',
    'geloofsbelydenis': r'geloof(sbelydenis)?',
}
re_split_stanza_list = re.compile(r'[,+(en)(and)&]')  # lists of stanzas are split using this regex
re_split_stanza_range = re.compile(r'[-–—]')  # the "from" and "to" parts of stanza ranges are split using this regex
re_stanzas = r'\d([\d\-–—:,+(en)(and)& ]*\d)?'
re_song_references = {name: re.compile(
    pattern=re_songbook_names[name] + '\s*(?P<stanzas>' + re_stanzas + ')?',
    flags=re.IGNORECASE,
) for name in re_songbook_names}


def parse_stanzas(reference: str) -> Tuple[int, List[int] or None]:
    """
    This is different from parsing chapters and verses of bible books, for two reasons:
    - We don't have to support crossing chapter bounaries, like 1:2-3:4
    - We have to support singing verses in arbitrary order, like 2:3,2,6-7

    :param reference: The human-readable song reference.

    :return: (song_number, list of stanza numbers)

    Single stanza
    >>> parse_stanzas("1:2")
    (1, [2])

    Comma-separated stanzas
    >>> parse_stanzas("1:2,3")
    (1, [2, 3])
    >>> parse_stanzas("1:2,5")
    (1, [2, 5])
    >>> parse_stanzas("1:2,5,6")
    (1, [2, 5, 6])

    Plus, &, 'en' and 'and' all mean the same thing as a comma:
    >>> parse_stanzas("95 : 1 + 2")
    (95, [1, 2])
    >>> parse_stanzas("95 : 1 + 2+3")
    (95, [1, 2, 3])
    >>> parse_stanzas("95 : 1, 2 en 3")
    (95, [1, 2, 3])
    >>> parse_stanzas("95 : 1, 2 and 3")
    (95, [1, 2, 3])
    >>> parse_stanzas("95 : 1, 2 & 3")
    (95, [1, 2, 3])

    Stanza ranges. Treat hypens and dashes of various lengths as the same thing.
    >>> parse_stanzas("1:2-3")
    (1, [2, 3])
    >>> parse_stanzas("1:2-5")  # hyphen
    (1, [2, 3, 4, 5])
    >>> parse_stanzas("1:2–5")  # en dash
    (1, [2, 3, 4, 5])
    >>> parse_stanzas("1:2—5")  # em dash
    (1, [2, 3, 4, 5])

    Ignore white-space
    >>> parse_stanzas("\t1 :2-  5    ")
    (1, [2, 3, 4, 5])

    Support repetition of verses
    >>> parse_stanzas("150:1,1")
    (150, [1, 1])

    Support mixture of ranges and individually listed verses
    >>> parse_stanzas("1:2-4,6-7")
    (1, [2, 3, 4, 6, 7])
    >>> parse_stanzas("1:6-7,4,3")
    (1, [6, 7, 4, 3])
    >>> parse_stanzas("1:6-7, 4, 3")
    (1, [6, 7, 4, 3])

    Use None to indicate that no stanzas were mentioned explicitly.
    This usually means we sing all stanzas.
    >>> parse_stanzas("1")
    (1, None)

    Complain when the given reference is something irrelevant
    >>> parse_stanzas("Genesis 5")
    Traceback (most recent call last):
    ...
    ValueError: Failed to parse: 'Genesis 5'
    >>> parse_stanzas("asdf")
    Traceback (most recent call last):
    ...
    ValueError: Failed to parse: 'asdf'
    >>> parse_stanzas(None)
    Traceback (most recent call last):
    ...
    ValueError: Failed to parse: None

    """

    try:
        try:
            song_number, the_rest = reference.split(':')
        except ValueError:
            return int(reference), None

        ranges = re_split_stanza_list.split(the_rest)
        stanza_numbers = []
        for r in ranges:
            if len(r):
                try:
                    from_verse, to_verse = re_split_stanza_range.split(r)
                    stanza_numbers.extend(range(int(from_verse), int(to_verse) + 1))
                except ValueError:
                    stanza_numbers.append(int(r))

        return int(song_number), stanza_numbers

    except (ValueError, AttributeError):
        raise ValueError("Failed to parse: {}".format(repr(reference)))


def which_songbook(reference: str) -> Tuple[str, str or None]:
    """

    :param reference: The human-readable song reference.

    :return:

    >>> which_songbook("Lied 512")
    ('lied', '512')
    >>> which_songbook("PS1:9")
    ('ps', '1:9')
    >>> which_songbook("Skrifberyming  17:2, 3")
    ('sb', '17:2, 3')
    >>> which_songbook("Gesange")
    ('ges', None)
    >>> which_songbook("5:6-7")
    Traceback (most recent call last):
    ...
    ValueError: Unknown songbook: '5:6-7'
    >>> which_songbook("sb50")
    ('sb', '50')
    >>> which_songbook("Psalm 5:7+9")
    ('ps', '5:7+9')
    >>> which_songbook("Psalm 5:7&9")
    ('ps', '5:7&9')
    >>> which_songbook("Psalm 5:7 and 9")
    ('ps', '5:7 and 9')
    >>> which_songbook("Psalm 5:7 en 9")
    ('ps', '5:7 en 9')
    """

    for songbook_name in re_song_references:
        regex = re_song_references[songbook_name]
        m = regex.match(reference)
        if m is not None:
            return songbook_name, m.group('stanzas')
    raise ValueError("Unknown songbook: {}".format(repr(reference)))


def parse_song_reference(reference: str) -> Tuple[str, int, List[int] or None]:
    """

    :param reference: The human-readable song reference.

    :return:

    >>> parse_song_reference("Lied512")
    ('lied', 512, None)
    >>> parse_song_reference("PS  1:9")
    ('ps', 1, [9])
    >>> parse_song_reference("Skrifberyming17:2, 3")
    ('sb', 17, [2, 3])
    >>> parse_song_reference("Gesange")
    Traceback (most recent call last):
    ...
    ValueError: No stanzas in: 'Gesange'
    >>> parse_song_reference("sb50")
    ('sb', 50, None)
    >>> parse_song_reference("Psalm 5:7+9")
    ('ps', 5, [7, 9])
    >>> parse_song_reference("Psalm 5:7&9")
    ('ps', 5, [7, 9])
    >>> parse_song_reference("Psalm 5:7 and 9")
    ('ps', 5, [7, 9])
    >>> parse_song_reference("Psalm 5:7 en 9")
    ('ps', 5, [7, 9])
    """

    songbook_name, stanza_string = which_songbook(reference)
    if stanza_string is None:
        raise ValueError("No stanzas in: {}".format(repr(reference)))
    song_number, stanza_numbers = parse_stanzas(stanza_string)
    return songbook_name, song_number, stanza_numbers
