import wx


def stacked(widgets, orient=wx.VERTICAL, proportion=0, flag=0) -> wx.BoxSizer:
    sizer = wx.BoxSizer(orient)
    for w in widgets:
        sizer.Add(w, proportion=proportion, flag=flag)
    return sizer


def titled(parent, widget, title: str, orient=wx.VERTICAL) -> wx.BoxSizer:
    """
    Take any control and put a title above or to the left of it
    """
    label = wx.StaticText(parent, label=title)
    sizer = wx.BoxSizer(orient)
    sizer.Add(label, flag=wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL)
    sizer.Add(widget, flag=wx.EXPAND, proportion=1)
    return sizer
